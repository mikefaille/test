#!/usr/bin/env bash

ansible-playbook -t before_reloading_EC2_inventory  rolling_upgrade.yaml

# To get ec2 inventory
# https://aws.amazon.com/blogs/apn/getting-started-with-ansible-and-dynamic-amazon-ec2-inventory-management/
export ANSIBLE_HOSTS=$PWD/ec2.py
export EC2_INI_PATH=$PWD/ec2.ini
./ec2.py --refresh-cache

ansible-playbook -t after_reloading_EC2_inventory  rolling_upgrade.yaml
