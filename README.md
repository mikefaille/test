# TEST CHALLENGE !
. By default, you may provision  VMSs
This apps is called test; because, someone try to test me and I like it :)

## Features

- Atomic updates. We can provision an EC2 template before propagate main config.
- Immutable infrastructure. We can reset our infra on known state.
- Infra as code. On our infra, we can do : Test, PR, versioning, rollback, automation, etc.
- Rolling upgrade. We can do fast and small release.
- It scales ! Automatic Group Scaler is used  + Application Load Balancer.
- It's resilient ! ""                ""
- It work ! test.agileops.sh
-  (Ok, let me configure the DNS... but, I have dynamic IP for now. It will provide them.
- It can be more secure. We actually have theses security features :
- Mysql connection limitation. Dedicated app user can join DB only from privates ips
- Unknown ports are closed in EC2 security group.
- Anti-feature : I don't provide ssl for now... I still like Let's Encrypt
- Idempotent. My ansible code for AWS try most of the time to use object ID and detect unwanted state.

## Software pre-requirements:

- Ansible 2.6+ (you can install it using virtualenv + pip)
- boto3 + botocore (pip)
- ssh-agent well configured with your key
- An IAM account with full access on VPC and EC2 AWS services. (explanations will come)


** Forlder layout :

```
.
├── "playbook"                          # Here our ansible work begin.
│   ├── "group_vars"                    # So wow, much configurations
│   ├── "roles"                         # Here we hide most of our automation.
│   ├── "ansible.cfg"                   # Permit me to add verb extension (do) on jinja.
│   ├── "aws_ec2_os.yaml"               # Playbook. After infra, we deploy OS+app.
│   ├── "aws_infra.yaml"                # Bootstrap and maintain VPC, SG, Net, EC2 templates
│   ├── "ec2.ini""                      # Follow me http://bit.ly/2NHmnav Please, refer to -->
│   ├── "ec2.py"                        # The best AWS inventory ever seen for Ansible.
│   ├── "rolling_upgrade.yaml"          # Playbook containing ALB, ASG and gentle upgrade code.
│   ├── "launch_rolling_upgrade.sh"   # Rolling upgrade launcher
│   └── "Vagrantfile"					# Used to boostrap our development. Now dead. :,(
├── "test2"								# Web site - Retro-geocities style
│   ├── "templates"						# Here reside your html templates
│   ├── "__init__.py"					# entry point for our flask application
│   ├── "names.sql"						# Used to bootstrap your DB (mysql)
│   └── "requirements.txt"              # We need few python libs. I pin them for my luck.
├── "conf.ini"                          # Config for my web app. Not in the Flask folder ? ¯\_(ツ)_/¯
└── "README.md"                         # You are Here ⮌
```


## Ansible usage


0. Because we use ansible on AWS, we need to configure boto credential.

```
[Credentials]
aws_access_key_id = XXXXXXXXXXxxxxxxxxx
aws_secret_access_key = XXXXXXXXXXXXXXXXXXX
region = XXXXXXXXXXXXXX
```

1. Take time to change configuration on : `./playbook/group_vars` . Please, report issues if you can't configure it.
2. Go on ansible (`./playbook`) folder and execute `./launch_rolling_upgrade.sh`
3. Drink a beer and appreciate.


## to dev using vagrant + ansible (deprecated) :

```
cd playbook/
vagrant up --provsion
```
