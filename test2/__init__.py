from flask import Flask, render_template
import pymysql

from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for
)



from flask import (
    request
)

import configparser


app = Flask(__name__)
app.secret_key = 'super secret key'
app.config['SESSION_TYPE'] = 'filesystem'



class Database:
    def __init__(self):

        config = configparser.SafeConfigParser()
        config.read_file(open('conf.ini'))
        host = config.get("DEFAULT", 'host', fallback="127.0.0.1")
        user = config.get("DEFAULT", 'user', fallback="myuser")
        password = config.get("DEFAULT", 'password', fallback="mypass")
        db = config.get("DEFAULT", 'db', fallback='db')

        self.con = pymysql.connect(host=host, user=user, password=password, db=db, cursorclass=pymysql.cursors.DictCursor)
        self.cur = self.con.cursor()

    def list_test(self):
        self.cur.execute("SELECT name_id, names, fav_color, cats_or_dogs FROM test LIMIT 50")
        result = self.cur.fetchall()

        return result

    def insert_test(self, names, fav_color, cats_or_dogs ):
        app.logger.info(names, fav_color, cats_or_dogs)
        self.cur.execute(
            "INSERT INTO test (`names`, `fav_color`, `cats_or_dogs`) VALUES (%s, %s, %s)",
            (names, fav_color ,  cats_or_dogs)

        )

        self.con.commit()


@app.route('/')
@app.route('/home')
def test():

    def db_query():
        db = Database()
        emps = db.list_test()

        return emps

    res = db_query()
    return render_template('test.html', result=res, content_type='application/json')


@app.route('/insert', methods=('GET', 'POST'))
def register():
    db = Database()
    if request.method == 'POST':
        names = request.form['names']
        fav_color = request.form['fav_color']
        cats_or_dogs = request.form['cats_or_dogs']
        db = Database()
        error = None

        # if not names:
        #     error = 'Name is required.'
        # elif db.cur.execute('SELECT * FROM test where names = %s', (names)) is not None:
        #     error = 'User {} is already insert.'.format(names)

        if error is None:
            db.insert_test(names, fav_color, cats_or_dogs)
            return redirect(url_for('test'))

        flash(error)

    return render_template('insert.html')
