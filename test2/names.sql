USE db;

SELECT 'CREATING DATABASE STRUCTURE' as 'INFO';


DROP TABLE IF EXISTS test;
/*!50503 set default_storage_engine = InnoDB */;
/*!50503 select CONCAT('storage engine: ', @@default_storage_engine) as INFO */;



CREATE TABLE test(
name_id     INT             NOT NULL UNIQUE  AUTO_INCREMENT,
       names        VARCHAR(16)      NOT NULL UNIQUE,
       fav_color   VARCHAR(16)       NOT NULL,
       cats_or_dogs VARCHAR(16) NOT NULL,
       PRIMARY KEY (name_id)
);
